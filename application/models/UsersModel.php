<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author raul
 */
class UsersModel extends CI_Model {

    public $us_id;
    public $us_name;
    public $us_lastname;
    public $us_hash;

    public function __construct() {
        $this->load->database();
        parent::__construct();
    }

    public function get() {
        $query = $this->db->get('users');
        return $query->result();
    }

    public function save() {
        $this->db->insert('users', $this);
    }



}

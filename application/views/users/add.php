<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Randock</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">


            <?php echo validation_errors(); ?>
            <?php echo form_open('users/add'); ?>         

            <div class="form-group">
                <label for="name">Firstname</label>
                <input type="text" class="form-control" name="name" placeholder="Firstname">
            </div>
            <div class="form-group">
                <label for="lastname">Lastname</label>
                <input type="text" class="form-control" name="lastname" placeholder="Lastname">
            </div>
            <button type="submit" class="btn btn-default" onClick="this.disabled=true; this.form.submit();">Submit</button>
        </form>
    </div>


</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Randock</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">
            <div class="alert alert-success" role="alert"> <strong>Success!</strong> Hi, <?=$response->firstname . ' ' . $response->lastname ?>, your hash is <?=$response->hash?>. Refresh go to index page. </div>
        </div>


    </body>
</html>
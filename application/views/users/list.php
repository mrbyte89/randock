<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Randock</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">
            <a href="<?php echo site_url('users/add') ?>">New</a>
            <?php if ($users) { ?>
                <div class="table-responsive">
                    <table class="table">
                        <?php foreach ($users as $user) { ?>
                            <tr>
                                <td><?= $user->us_name ?></td>
                                <td><?= $user->us_lastname ?></td>
                                <td><?= $user->us_hash ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php } else { ?>
                No hay usuarios dados de alta.
            <?php } ?>
        </div>


    </body>
</html>
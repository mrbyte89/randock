<?php

/**
 * Created by PhpStorm.
 * User: raul
 * Date: 4/12/16
 * Time: 12:03
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

    public function index() {
        $this->load->helper('form');
        $this->load->view('welcome_message');
    }

    public function login() {
        if ($this->input->method(TRUE) === "POST") {
            if ($this->input->post('email') === USER_PANEL && $this->input->post('password') === PWD_PANEL) {
                $this->load->library('session');
                $this->session->start = true;
                redirect('/users/add');
            } else {
                redirect('/authentication/index');
            }
        }
    }

}

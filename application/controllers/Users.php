<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users
 *
 * @author raul
 */
class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');

        if (!$this->session->start) {
            redirect('/authentication/index');
        }

        $this->load->model('usersmodel');
        $this->load->helper('url_helper');
    }

    public function index() {
        $this->load->view('users/list', array(
            'users' => $this->usersmodel->get()
        ));
    }

    public function add() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('lastname', 'Lastname', 'required');


        if ($this->form_validation->run() === FALSE) {
            $this->load->view('users/add');
        } else {

            $this->load->library('randockapi');


            $API = new RandockAPI();

            $user = new UsersModel();
            $user->us_name = $this->input->post('name');
            $user->us_lastname = $this->input->post('lastname');

            $hash = $API->getHash(array(
                'firstname' => $user->us_name,
                'lastname' => $user->us_lastname
            ));

            if (isset($hash->hash)) {
                $user->us_hash = $hash->hash;
                $user->save();
                $this->session->set_flashdata('user', array(
                    'firstname' => $user->us_name,
                    'lastname' => $user->us_lastname,
                    'hash' => $user->us_hash
                ));
                redirect('/users/success');
            } else {
                redirect('/users/error');
            }
        }
    }

    public function success() {
        $data = (object) $this->session->flashdata('user');
        $this->session->sess_destroy();
        $this->load->view('users/success', array(
            'response' => $data
        ));
    }

    public function error() {
        $this->session->sess_destroy();
        $this->load->view('users/error');
    }

}
